// swift-tools-version: 5.6

import PackageDescription

let package = Package(
	name: "FolioReaderKit",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "FolioReaderKit",
            targets: ["FolioReaderKit"]
        ),
    ],
	dependencies: [
        .package(url: "https://github.com/ZipArchive/ZipArchive.git", exact: "2.4.3"),
        .package(url: "https://github.com/cxa/MenuItemKit.git", from: "3.1.3"),
        .package(url: "https://gitlab.com/exoplanet-digital-public/projects/zfdragablemodaltransition-ios", branch: "master"),
        .package(url: "https://github.com/tadija/AEXML.git", branch: "master"),
        .package(url: "https://github.com/ArtSabintsev/FontBlaster.git", from: "5.1.0"),
        .package(url: "https://github.com/realm/realm-swift", exact: "3.17.3")
    ],
    targets: [
        .target(
            name: "FolioReaderKit",
            dependencies: [
                "ZipArchive",
                "MenuItemKit",
                .product(name: "ZFDragableModalTransition", package: "zfdragablemodaltransition-ios"),
                "AEXML",
                "FontBlaster",
                .product(name: "RealmSwift", package: "realm-swift")
            ],
            path: "Source",
            resources: [.process("Resources")]
        ),
        .testTarget(
            name: "FolioReaderKitTests",
            dependencies: ["FolioReaderKit"],
            path: "FolioReaderKitTests"
        ),
    ]
)
